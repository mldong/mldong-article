## 前言

在先导篇中已经提到前端所用技术栈，这里算是入门篇，项目启动，搭建脚手架。

 这里使用vue-admin-template而不是vue-element-admin是原因是前者会更简洁，熟悉起来会更简单些。

## 开始动手

``` shell
# 克隆项目
git clone https://github.com/PanJiaChen/vue-admin-template.git mlong-vue
# 进入项目
cd mldong-vue
# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

 浏览器访问` http://localhost:9528/ `

## 工程说明

### 目录结构

>  这里只罗列个大概，未全部。

``` 
├── build  项目构建配置
├── public  打包所需静态资源
└── src
    ├── api  AJAX请求
    └── assets  项目静态资源
    ├── components  自定义组件
    ├── icons  图标
    ├── layout  布局文件
    ├── router  路由文件
    ├── store  vuex状态管理
    ├── styles  样式
    ├── utils  工具包
    ├── views  页面文件
    ├── App.vue  入口Vue文件
    ├── main.js  启动入口
    ├── permision.js  权限相关
    ├── settings.js  配置项
    └── test  测试相关
├── .env.development  开发配置
├── .env.production  生产配置
├── .env.staging  mock配置
├── .eslintignore  忽略eslint校验规则的配置
├── .eslint.js  eslint规则配置
├── .gitignore  git忽略
├── package-lock.json  包
├── package.json  包依赖配置
└── vue.config.json  vue-cli配置
```

### package.json文件说明

```
{
  "scripts": {       // 这里的脚本都可以通过npm run xxx去执行
    "dev": "vue-cli-service serve",		// 对应上面的npm run dev
    "build:prod": "vue-cli-service build",// npm run build:prod 打生产包
    "build:stage": "vue-cli-service build --mode staging", // 注意一下这里的--mode staging,一会儿会说明
    ......
  },
  "dependencies": {
    "axios": "0.18.1", // 开发环境和生产环境都能使用的包，通过命令npm instal xx --save 安装
    .....
  },
  "devDependencies": {
    "@babel/core": "7.0.0",// 仅开发环境能使用的包，通过命令npm instal xx --save-dev 安装
    .....
  }
}
```

### .env文件说明

* .env.development

``` 
# just a flag
ENV = 'development'
# base api
VUE_APP_BASE_API = '/dev-api'

VUE_CLI_BABEL_TRANSPILE_MODULES = true

```

* .env.production

``` 
# just a flag
ENV = 'production'
# base api
VUE_APP_BASE_API = '/prop-api'

```

* .env.staging

``` NODE_ENV = production
NODE_ENV = production

# just a flag
ENV = 'staging'

# base api
VUE_APP_BASE_API = '/stage-api'
```

```
# 这里对应的启用的就是.env.staging文件，如果需要新增别的环境的配置，可以复制一份.env.staging
vue-cli-service build --mode staging
```

### 关于.env配置文件使用

可以看一下src/utils/request.js

``` javascript
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})
```

如果启用的是.env.staging，则以上代码等价于

``` javascript
const service = axios.create({
  baseURL: '/stage-api', // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})
```

注意的是，它会连单引号也一起，原样替换。

### 关于.env配置文件中的NODE_ENV

``` 
npm run dev
# =====>NODE_ENV=development     隐式指定

npm run build
# =====>NODE_ENV=production		隐式指定

npm run build --mode staging
# =====>NODE_ENV=production     需要显式指定

```

webpack打包，是根据NODE_ENV是开发还是生产来判断打包的方式的，各自打包的大小会不一样。新增的.env文件，需要显式指定NODE_ENV为production。

### vue.config.js文件说明

以下截取的是代码片段

```javascript
'use strict'
const path = require('path')
const defaultSettings = require('./src/settings.js')

function resolve(dir) {
  return path.join(__dirname, dir)
}
// src/settings.js 配置有，就使用settings的title,没有就使用右边的
// 生成对应的是public/index.html 的<title></title>
const name = defaultSettings.title || 'vue Admin Template' // page title

// 开发模式下的端口
const port = process.env.port || process.env.npm_config_port || 9528 // dev port

// All configuration item explanations can be find in https://cli.vuejs.org/config/
module.exports = {

  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development', // 是否使用eslint
  /**
   * 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
   *  打包之后发现map文件过大，项目文件体积很大，设置为false就可以不输出map文件
   *  map文件的作用在于：项目打包后，代码都是经过压缩加密的，如果运行时报错，输出的错误信息无法准确得知是哪	里的代码报错。
   *  有了map就可以像未加密的代码一样，准确的输出是哪一行哪一列有错。
   * */
  productionSourceMap: false,
  devServer: {
    port: port, // 开发模式下的端口
    open: true, // 是否自动打开浏览器
    overlay: {
      warnings: true, // 开启这个，这样eslint报错会显示在浏览器上
      errors: true
    },
    proxy: {		// 代理配置
      '/api': {
        target: 'http://api.mldong.com',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/api': '/'          // 以/api开头的，全部指定到target+ '/'上
        }               // http://localhost:9528/api/login ==>http://api.mldong.com/login
      }
    },
    before: require('./mock/mock-server.js')
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')   // 目录别名 
        // import App from 'src/App.vue' ==> import App from '@/App.vue'
      }
    }
  }
}
```

### .eslintrc.js文件说明

> eslint规则配置，强烈建议开启eslint配置！！！

以下截取的是代码片段

```
module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  extends: ['plugin:vue/recommended', 'eslint:recommended'],
  rules: {
    "vue/max-attributes-per-line": [2, {
      "singleline": 10,
      "multiline": {
        "max": 1,
        "allowFirstLine": false
      }
    }],
    // 强列建议开启eslint校验，规范前端代码，
    // vscode的eslint插件会有对应的错误提示，如果有哪些不习惯的，可通过配置为off来关闭
    "vue/singleline-html-element-content-newline": "off",
    "vue/multiline-html-element-content-newline":"off",
    "vue/html-closing-bracket-newline": "off",
    ...
  }
}

```

## 加入版本库，推送到git仓库

略

## 项目源码地址

- 后端

 https://gitee.com/mldong/mldong

- 前端

 https://gitee.com/mldong/mldong-vue

## 相关文章

 [打造一款适合自己的快速开发框架-先导篇](https://juejin.im/post/5eca0304518825432978055c) 

 [打造一款适合自己的快速开发框架-前端篇之登录与路由模块化](https://juejin.im/post/5eeb877bf265da02be0ce628) 

 [打造一款适合自己的快速开发框架-前端篇之框架分层及CURD样例](https://juejin.im/post/5eed4f81f265da02c94e1346) 

 [打造一款适合自己的快速开发框架-前端篇之字典组件设计与实现](https://juejin.im/post/5ef2233ce51d45741e436c11) 

 [打造一款适合自己的快速开发框架-前端篇之下拉组件设计与实现](https://juejin.im/post/5ef43301f265da22cb481369) 

 [打造一款适合自己的快速开发框架-前端篇之选择树组件设计与实现](https://juejin.im/post/5ef563185188252e9a1fe26a) 

 [打造一款适合自己的快速开发框架-前端篇之代码生成器](https://juejin.im/post/5ef6a6b16fb9a07e693a5eae) 

 [打造一款适合自己的快速开发框架-前端篇之权限管理](https://juejin.im/post/5efcb274f265da22df3cc314) 