## 前言

一般来说每一个系统都会有权限管理，而基于rbac是最常用的一种方式，本快速开发框架也是采用rbac，不过在权限资源的管理上和传统的做法有些不一样。

## 定义

 rbac全称叫做基于角色权限控制（ Role-Based Access Control ） 。 让角色绑定权限,用户绑定角色,它们之间都是多对多的关系 。

### 权限资源

在前后端分离后，我把权限资源分为如下三种：

1. API接口

   前后端分离放弃了传统的服务端渲染的方式，后端只需提供API接口。此时，每一个控制层方法即为一种权限资源。

2. 路由菜单

   路由菜单即前端所需要的路由 ， 组织起一个应用的关键骨架。

3. 页面按钮

   前端页面上的操作按钮

### 角色

系统的管理角色，每一个角色可以有多个管理用户。

### 用户

系统的管理用户，一个用户可以有多个角色。

## 权限资源的管理方式 

在传统的做法中，上面说到的三类权限资源都是入库管理的。在这里和传统的做法有些许不同，如下分别为这三类资源采用的管理方式：

| 资源类型 | 管理方式                                                     |
| -------- | ------------------------------------------------------------ |
| API接口  | 看过之前CURD模板的同学可能知道，我们的控制层的类和方法上都是有注解的，对应的是springmvc和swaggerui的注解，所以我们可以在系统启动的时候，将这些注解的数据重新组装，构成权限资源供后端勾选分配。 |
| 路由菜单 | 路由菜单对应的是前端的菜单，这里还是会和传统的做法一样，后台管理页面提供人工录入。不过可以提供给超级管理同步前端路由菜单的接口，点击同步后，可以将前端的路由基础数据同步到后端，省去了部分人工维护的工作量。 |
| 页面按钮 | 页面上的操作按钮一般都会对应着后端的一个接口资源，所以这里不单独维护，只需要前端按钮权限标识规范，给页面按钮加上标识即可。 |

## Swaggerui权限注解详解

可能来个代码片段会更直接些

``` java
@RestController
@RequestMapping("/sys/role")
@Api(tags="sys-角色管理",authorizations={
    @Authorization(value="sys|系统管理",scopes={
    	@AuthorizationScope(description="角色管理",scope="sys:role:index")
    })
})
public class SysRoleController {
	@Autowired
	private SysRoleService sysRoleService;

	@PostMapping("save")
	@ApiOperation(value="添加角色", notes="添加角色",authorizations={
		@Authorization(value="添加角色",scopes={
	    	@AuthorizationScope(description="添加角色",scope="sys:role:save")
	    })
	})
	public CommonResult<?> save() {
	}
}
```

Swaggerui有权限注解,所以可以充分利用上,我把权限资源分为三级:模块、功能、方法,分别对应上面的sys|系统管理、角色管理、添加角色。一个完整的CURD，会得到的权限树结构如下：

``` 
sys|系统管理
    - 角色管理
    	- 添加角色
    	- 修改角色
    	- 删除角色
    	- 通过id获取角色
        - 分页查询角色
```



   ## 数据库设计

### ER图

略

### 表设计

- 用户表(sys_user)

``` mysql
CREATE TABLE `sys_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_name` varchar(32) NOT NULL COMMENT '用户名',
  `real_name` varchar(32) DEFAULT NULL COMMENT '姓名',
  `avatar` varchar(200) DEFAULT NULL COMMENT '头像',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `mobile_phone` varchar(11) DEFAULT NULL COMMENT '手机号',
  `telephone` varchar(20) DEFAULT NULL COMMENT '电话',
  `password` varchar(40) DEFAULT NULL COMMENT '密码',
  `salt` varchar(10) DEFAULT NULL COMMENT '加盐',
  `sex` int(6) unsigned DEFAULT '1' COMMENT '性别(1->男|MALE,2->女|FEMALE,3->未知|UNKNOWN)',
  `is_locked` tinyint(1) unsigned DEFAULT '2' COMMENT '是否锁定(1->已锁定|YES,2->未锁定|NO)',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(3) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除(1->未删除|NO,2->已删除|YES)',
  PRIMARY KEY (`id`),
  KEY `real_name` (`real_name`),
  KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='用户';

```

- 角色表(sys_role)

``` mysql
CREATE TABLE `sys_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) CHARACTER SET utf8mb4 NOT NULL COMMENT '角色名称',
  `role_key` varchar(32) DEFAULT NULL COMMENT '角色标识(唯一)',
  `role_type` int(6) DEFAULT '10' COMMENT '角色类型(10->管理员|ADMIN,20->流程审核员|WORKFLOW)',
  `is_enabled` tinyint(1) DEFAULT '2' COMMENT '是否启用(1->禁用|NO,2->启用|YES)',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(3) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除(1->未删除|YES,2->已删除|NO)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色';

```

- 用户角色关系表(sys_user_role)

``` mysql
CREATE TABLE `sys_user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) unsigned DEFAULT NULL COMMENT '用户id',
  `role_id` bigint(20) unsigned DEFAULT NULL COMMENT '角色id',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(3) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除(1->未删除|NO,2->已删除|YES)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户角色关系表';
```

- 角色资源关系表(sys_role_access)

``` mysql
CREATE TABLE `sys_role_access` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` bigint(20) unsigned DEFAULT NULL COMMENT '角色id',
  `access` varchar(64) DEFAULT NULL COMMENT '权限标识',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(3) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) DEFAULT '1' COMMENT '是否删除(1->未删除|NO,2->已删除|YES)',
  PRIMARY KEY (`id`),
  KEY `idx_sys_role_access` (`access`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色权限关系';


```

- 路由菜单表(sys_menu)

``` mysql
CREATE TABLE `sys_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` bigint(20) unsigned DEFAULT '0' COMMENT '父菜单id',
  `name` varchar(64) DEFAULT NULL COMMENT '菜单名称',
  `sort` double(10,2) DEFAULT '10.00' COMMENT '排序',
  `route_name` varchar(64) DEFAULT NULL COMMENT '路由名称',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `is_show` tinyint(1) unsigned DEFAULT '2' COMMENT '是否显示(1->不显示|NO,2->显示|YES)',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(3) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除(1->未删除|NO,2->已删除|YES)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单';
```

- 角色菜单关系表(sys_role_menu)

   ``` mysql
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `menu_id` bigint(20) unsigned NOT NULL COMMENT '菜单id',
  `create_time` datetime(3) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(3) DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) unsigned DEFAULT '1' COMMENT '是否删除(1->未删除|NO,2->已删除|YES)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色菜单关系';
  ```

## 开始编码

### 目录结构

``` 
├── mldong-admin  管理端接口
	├── src/main/java
		├──	com.mldong.modules.sys
			├── controller
				├──	SysMenuController.java
				├──	SysRbacController.java
				├──	SysRoleController.java
				└── SysUserController.java
			├── dao
				└── SysUserDao.java
			├──	dto
				├──	SysMenuPageParam.java
				├──	SysMenuParam.java
				├──	SysRolePageParam.java
				├──	SysRoleParam.java
				└──	SysUserPageParam.java
				└──	SysUserParam.java
			└──	service
				├── impl
					├──	SysMenuServiceImpl.java
                    ├──	SysRbacServiceImpl.java
                    ├──	SysRoleServiceImpl.java
                    └── SysUserServiceImpl.java
				├──	SysMenuService.java
                ├──	SysRbacService.java
                ├──	SysRoleService.java
                └── SysUserService.java
	├── src/main/resources
		└── dao/sys
			└── sys_user_dao.xml
├── mldong-common  工具类及通用代码
	├── src/main/java
		├──	com.mldong.common
			├── access
				├──	model
					└── SysAccessModel.java
				└── AccessInitProcessor.java
			├── base
				└── IdAndIdsParam.java
├── mldong-generator  代码生成器
```

   ### 核心文件说明：

- `mldong-common/src/main/java/com/mldong/common/access/model/SysAccessModel.java`

权限资源模型，即由权限注解转化后的实体类

``` java
package com.mldong.common.access.model;

import java.io.Serializable;
import java.util.List;
/**
 * 权限资源模型
 * @author mldong
 *
 */
public class SysAccessModel implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5248234808496873095L;
	/**
	 * 主键，这里和access一至
	 */
	private String id;
	/**
	 * 权限标识，与请求uri对应,/sys/user/save===>sys:user:save
	 */
	private String access;
	/**
	 * 请求uri，与RequestMapper对应
	 */
	private String uri;
	/**
	 * 权限名称
	 */
	private String name;
	/**
	 * 权限备注
	 */
	private String remark;
	/**
	 * 排序,暂不实现
	 */
	private int sort;
	/**
	 * 子权限
	 */
	private List<SysAccessModel> children;
	// 省略 get set
	
}

```

- `mldong-common/src/main/java/com/mldong/common/access/AccessInitProcessor.java`

控制类权限处理器(主要将注解转成权限资源模型)，实现spring的BeanPostProcessor接口的postProcessAfterInitialization方法。

``` java
package com.mldong.common.access;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import com.mldong.common.access.model.SysAccessModel;
/**
 * 控制类权限处理器(主要将注解转成权限资源模型)
 * @author mldong
 *
 */
@Component
public class AccessInitProcessor implements BeanPostProcessor{
	/**
	 * 权限集合
	 */
	private List<SysAccessModel> accessList = new ArrayList<>();
	/**
	 * 模块权限
	 */
	private Map<String,SysAccessModel> moduleMap = new HashMap<>();
	@Override
	public Object postProcessBeforeInitialization(Object bean,
			String beanName) throws BeansException {
		return bean;
	}
	@Override
	public Object postProcessAfterInitialization(Object bean,
			String beanName) throws BeansException {
		if(beanName.endsWith("Controller")) {
			Api api = bean.getClass().getAnnotation(Api.class);
			if(api==null) {
				return bean;
			}
			Authorization[] authorizations = api.authorizations();
			if(authorizations.length>0) {
				Authorization authorization = authorizations[0];
				// 处理模块
				SysAccessModel moduleAccess = moduleMap.get(authorization.value());
				if(null == moduleAccess) {
					moduleAccess = handleAuthorization(authorization);
					moduleAccess.setChildren(new ArrayList<>());
					accessList.add(moduleAccess);
					moduleMap.put(authorization.value(), moduleAccess);
				}
				// 处理控制类
				AuthorizationScope[] authorizationScopes = authorization.scopes();
				if(authorizations.length>0) {
					AuthorizationScope authorizationScope = authorizationScopes[0];
					SysAccessModel controllerAccess = handleAuthorizationScope(authorizationScope);
					controllerAccess.setChildren(new ArrayList<>());
					moduleAccess.getChildren().add(controllerAccess);
					// 处理方法
					Method[] methods = bean.getClass().getMethods();
					for (Method method : methods) {
						ApiOperation apiOperation = method.getAnnotation(ApiOperation.class);
						if(apiOperation==null) {
							continue;
						}
						authorizations = apiOperation.authorizations();
						if(authorizations.length>0) {
							authorization = authorizations[0];
							authorizationScopes = authorization.scopes();
							if(authorizations.length>0) {
								authorizationScope = authorizationScopes[0];
								SysAccessModel methodAccess = handleAuthorizationScope(authorizationScope);
								controllerAccess.getChildren().add(methodAccess);
							}
						}
					}
				}
			}
		}
		return bean;
	}
	/**
	 * 处理权限模块
	 * @param authorization
	 */
	private SysAccessModel handleAuthorization(Authorization authorization) {
		SysAccessModel moduleAccess = new SysAccessModel();
		String value = authorization.value();
		String arr [] = value.split("\\|");
		if(arr.length==2) {
			moduleAccess.setId(arr[0]);
			moduleAccess.setAccess(arr[0]);
			moduleAccess.setName(arr[1]);
			moduleAccess.setRemark(arr[1]);
		} else {
			moduleAccess.setId(value);
			moduleAccess.setAccess(value);
			moduleAccess.setName(value);
			moduleAccess.setRemark(value);
		}
		moduleAccess.setUri("/"+moduleAccess.getAccess().replaceAll(":", "/"));
		return moduleAccess;
	}
	/**
	 * 处理控制类
	 * @param authorizationScope
	 */
	private SysAccessModel handleAuthorizationScope(AuthorizationScope authorizationScope) {
		SysAccessModel access = new SysAccessModel();
		String scope = authorizationScope.scope();
		String descripion = authorizationScope.description();
		access.setId(scope);
		access.setAccess(scope);
		access.setUri("/"+scope.replaceAll(":", "/"));
		access.setName(descripion);
		access.setRemark(descripion);
		return access;
	}
	public List<SysAccessModel> getAccessList() {
		return accessList;
	}
}
```

- `mldong-admin/src/main/java/com/mldong/modules/sys/service/SysRbacService.java`

权限管理相关接口定义

  ``` java
package com.mldong.modules.sys.service;

import java.util.List;

import com.mldong.common.access.model.SysAccessModel;
import com.mldong.common.base.CommonPage;
import com.mldong.common.base.IdAndIdsParam;
import com.mldong.modules.sys.dto.SysUserWithRoleIdPageParam;
import com.mldong.modules.sys.entity.SysMenu;
import com.mldong.modules.sys.entity.SysUser;

/**
 * 权限管理相关接口
 * @author mldong
 *
 */
public interface SysRbacService {
	/**
	 * 获取权限树
	 * @return
	 */
	public List<SysAccessModel> listAccessTree();
	/**
	 * 角色成员列表
	 * @param param
	 * @return
	 */
	public CommonPage<SysUser> listUserByRoleId(SysUserWithRoleIdPageParam param);
	/**
	 * 保存用户角色关系
	 * @param param
	 * @return
	 */
	public int saveUserRole(IdAndIdsParam param);
	/**
	 * 从角色中移除用户
	 * @param param
	 * @return
	 */
	public int deleteUserRole(IdAndIdsParam param);
	/**
	 * 查询未加入指定角色的用户列表
	 * @param param
	 * @return
	 */
	public CommonPage<SysUser> listUserNoInRole(SysUserWithRoleIdPageParam param);
	/**
	 * 保存角色权限资源关系
	 * @param param
	 * @return
	 */
	public int saveRoleAccess(IdAndIdsParam param);
	/**
	 * 删除角色权限资源关系
	 * @param param
	 * @return
	 */
	public int deleteRoleAccess(IdAndIdsParam param);
	/**
	 * 保存角色菜单关系
	 * @param param
	 */
	public int saveRoleMenu(IdAndIdsParam param);
	/**
	 * 删除角色菜单关系
	 * @param param
	 * @return
	 */
	public int deleteRoleMenu(IdAndIdsParam param);
	/**
	 * 判断用户是否有权限
	 * @param userId 用户id
	 * @param access 权限标识
	 * @return
	 */
	public boolean hasAccess(Long userId,String access);
	/**
	 * 加载用户权限资源标识
	 * @param userId
	 * @return
	 */
	public List<String> loadUserAccessList(Long userId);
	/**
	 * 加载用户菜单权限
	 * @param userId
	 * @return
	 */
	public List<SysMenu> loadUserMenuList(Long userId);
}
  ```

- `mldong-admin/src/main/java/com/mldong/modules/sys/controller/SysRbacController.java`

权限管理控制类

   ``` java
package com.mldong.modules.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mldong.common.access.model.SysAccessModel;
import com.mldong.common.base.CommonPage;
import com.mldong.common.base.CommonResult;
import com.mldong.common.base.IdAndIdsParam;
import com.mldong.modules.sys.dto.SysUserWithRoleIdPageParam;
import com.mldong.modules.sys.entity.SysUser;
import com.mldong.modules.sys.service.SysRbacService;

@RestController
@RequestMapping("/sys/rbac")
@Api(tags="sys-权限管理",authorizations={
    @Authorization(value="sys|系统管理",scopes={
    	@AuthorizationScope(description="权限管理",scope="sys:rbac:index")
    })
})
public class SysRbacController {
	@Autowired
	private SysRbacService sysRbacService;

	@PostMapping("listAccessTree")
	@ApiOperation(value="获取权限资源树", notes="获取权限资源树",authorizations={
		@Authorization(value="获取权限资源树",scopes={
	    	@AuthorizationScope(description="获取权限资源树",scope="sys:role:listAccessTree")
	    })
	})
	public CommonResult<List<SysAccessModel>> listAccessTree() {
		return CommonResult.success("获取权限资源树",sysRbacService.listAccessTree());
	}

	@PostMapping("listUserByRoleId")
	@ApiOperation(value="角色成员列表", notes="角色成员列表",authorizations={
		@Authorization(value="角色成员列表",scopes={
	    	@AuthorizationScope(description="角色成员列表",scope="sys:role:listUserByRoleId")
	    })
	})
	public CommonResult<CommonPage<SysUser>> listUserByRoleId(@RequestBody @Validated SysUserWithRoleIdPageParam param) {
		return CommonResult.success("角色成员列表",sysRbacService.listUserByRoleId(param));
	}

	@PostMapping("listUserNoInRole")
	@ApiOperation(value="查询未加入指定角色的用户列表", notes="查询未加入指定角色的用户列表",authorizations={
		@Authorization(value="查询未加入指定角色的用户列表",scopes={
	    	@AuthorizationScope(description="查询未加入指定角色的用户列表",scope="sys:role:listUserNoInRole")
	    })
	})
	public CommonResult<CommonPage<SysUser>> listUserNoInRole(@RequestBody @Validated SysUserWithRoleIdPageParam param) {
		return CommonResult.success("查询未加入指定角色的用户列表",sysRbacService.listUserNoInRole(param));
	}

	@PostMapping("saveUserRole")
	@ApiOperation(value="保存用户角色关系", notes="保存用户角色关系",authorizations={
		@Authorization(value="保存用户角色关系",scopes={
	    	@AuthorizationScope(description="保存用户角色关系",scope="sys:role:saveUserRole")
	    })
	})
	public CommonResult<?> saveUserRole(@RequestBody @Validated IdAndIdsParam param) {
		int count = sysRbacService.saveUserRole(param);
		if(count>0) {
			return CommonResult.success("保存用户角色关系成功", null);
		} else {
			return CommonResult.fail("保存用户角色关系失败", null);
		}
	}

	@PostMapping("deleteUserRole")
	@ApiOperation(value="从角色中移除用户", notes="从角色中移除用户",authorizations={
		@Authorization(value="从角色中移除用户",scopes={
	    	@AuthorizationScope(description="从角色中移除用户",scope="sys:role:deleteUserRole")
	    })
	})
	public CommonResult<?> deleteUserRole(@RequestBody @Validated IdAndIdsParam param) {
		int count = sysRbacService.deleteUserRole(param);
		if(count>0) {
			return CommonResult.success("从角色中移除用户成功", null);
		} else {
			return CommonResult.fail("从角色中移除用户失败", null);
		}
	}
	@PostMapping("saveRoleAccess")
	@ApiOperation(value="保存角色权限资源关系", notes="保存角色权限资源关系",authorizations={
		@Authorization(value="保存角色权限资源关系",scopes={
	    	@AuthorizationScope(description="保存角色权限资源关系",scope="sys:role:saveRoleAccess")
	    })
	})
	public CommonResult<?> saveRoleAccess(@RequestBody @Validated IdAndIdsParam param) {
		int count = sysRbacService.saveRoleAccess(param);
		if(count>0) {
			return CommonResult.success("保存角色权限资源关系成功", null);
		} else {
			return CommonResult.fail("保存角色权限资源关系失败", null);
		}
	}
	@PostMapping("deleteRoleAccess")
	@ApiOperation(value="删除角色权限资源关系", notes="删除角色权限资源关系",authorizations={
		@Authorization(value="删除角色权限资源关系",scopes={
	    	@AuthorizationScope(description="删除角色权限资源关系",scope="sys:role:deleteRoleAccess")
	    })
	})
	public CommonResult<?> deleteRoleAccess(@RequestBody @Validated IdAndIdsParam param) {
		int count = sysRbacService.deleteRoleAccess(param);
		if(count>0) {
			return CommonResult.success("保存角色权限资源关系成功", null);
		} else {
			return CommonResult.fail("保存角色权限资源关系失败", null);
		}
	}
	@PostMapping("saveRoleMenu")
	@ApiOperation(value="保存角色菜单关系", notes="保存角色菜单关系",authorizations={
		@Authorization(value="保存角色菜单关系",scopes={
	    	@AuthorizationScope(description="保存角色菜单关系",scope="sys:role:saveRoleMenu")
	    })
	})
	public CommonResult<?> saveRoleMenu(@RequestBody @Validated IdAndIdsParam param) {
		int count = sysRbacService.saveRoleMenu(param);
		if(count>0) {
			return CommonResult.success("保存角色菜单关系成功", null);
		} else {
			return CommonResult.fail("保存角色菜单关系失败", null);
		}
	}
	@PostMapping("deleteRoleMenu")
	@ApiOperation(value="删除角色菜单关系", notes="删除角色菜单关系",authorizations={
		@Authorization(value="删除角色菜单关系",scopes={
	    	@AuthorizationScope(description="删除角色菜单关系",scope="sys:role:deleteRoleMenu")
	    })
	})
	public CommonResult<?> deleteRoleMenu(@RequestBody @Validated IdAndIdsParam param) {
		int count = sysRbacService.deleteRoleMenu(param);
		if(count>0) {
			return CommonResult.success("删除角色菜单关系成功", null);
		} else {
			return CommonResult.fail("删除角色菜单关系失败", null);
		}
	}
}

   ```

### 最终生成的权限树

``` json
{
  "code": 0,
  "msg": "获取权限资源树",
  "data": [
    {
      "id": "sys",
      "access": "sys",
      "uri": "/sys",
      "name": "系统管理",
      "remark": "系统管理",
      "sort": 0,
      "children": [
        {
          "id": "sys:menu:index",
          "access": "sys:menu:index",
          "uri": "/sys/menu/index",
          "name": "菜单管理",
          "remark": "菜单管理",
          "sort": 0,
          "children": [
            {
              "id": "sys:menu:remove",
              "access": "sys:menu:remove",
              "uri": "/sys/menu/remove",
              "name": "删除菜单",
              "remark": "删除菜单",
              "sort": 0
            },
            {
              "id": "sys:menu:get",
              "access": "sys:menu:get",
              "uri": "/sys/menu/get",
              "name": "通过id获取菜单",
              "remark": "通过id获取菜单",
              "sort": 0
            },
            {
              "id": "sys:menu:update",
              "access": "sys:menu:update",
              "uri": "/sys/menu/update",
              "name": "修改菜单",
              "remark": "修改菜单",
              "sort": 0
            },
            {
              "id": "sys:menu:list",
              "access": "sys:menu:list",
              "uri": "/sys/menu/list",
              "name": "分页查询菜单",
              "remark": "分页查询菜单",
              "sort": 0
            },
            {
              "id": "sys:menu:save",
              "access": "sys:menu:save",
              "uri": "/sys/menu/save",
              "name": "添加菜单",
              "remark": "添加菜单",
              "sort": 0
            }
          ]
        },
        {
          "id": "sys:rbac:index",
          "access": "sys:rbac:index",
          "uri": "/sys/rbac/index",
          "name": "权限管理",
          "remark": "权限管理",
          "sort": 0,
          "children": [
            {
              "id": "sys:role:listUserByRoleId",
              "access": "sys:role:listUserByRoleId",
              "uri": "/sys/role/listUserByRoleId",
              "name": "角色成员列表",
              "remark": "角色成员列表",
              "sort": 0
            },
            {
              "id": "sys:role:listAccessTree",
              "access": "sys:role:listAccessTree",
              "uri": "/sys/role/listAccessTree",
              "name": "获取权限资源树",
              "remark": "获取权限资源树",
              "sort": 0
            },
            {
              "id": "sys:role:listUserNoInRole",
              "access": "sys:role:listUserNoInRole",
              "uri": "/sys/role/listUserNoInRole",
              "name": "查询未加入指定角色的用户列表",
              "remark": "查询未加入指定角色的用户列表",
              "sort": 0
            },
            {
              "id": "sys:role:saveUserRole",
              "access": "sys:role:saveUserRole",
              "uri": "/sys/role/saveUserRole",
              "name": "保存用户角色关系",
              "remark": "保存用户角色关系",
              "sort": 0
            },
            {
              "id": "sys:role:deleteUserRole",
              "access": "sys:role:deleteUserRole",
              "uri": "/sys/role/deleteUserRole",
              "name": "从角色中移除用户",
              "remark": "从角色中移除用户",
              "sort": 0
            },
            {
              "id": "sys:role:saveRoleAccess",
              "access": "sys:role:saveRoleAccess",
              "uri": "/sys/role/saveRoleAccess",
              "name": "保存角色权限资源关系",
              "remark": "保存角色权限资源关系",
              "sort": 0
            },
            {
              "id": "sys:role:deleteRoleAccess",
              "access": "sys:role:deleteRoleAccess",
              "uri": "/sys/role/deleteRoleAccess",
              "name": "删除角色权限资源关系",
              "remark": "删除角色权限资源关系",
              "sort": 0
            },
            {
              "id": "sys:role:saveRoleMenu",
              "access": "sys:role:saveRoleMenu",
              "uri": "/sys/role/saveRoleMenu",
              "name": "保存角色菜单关系",
              "remark": "保存角色菜单关系",
              "sort": 0
            },
            {
              "id": "sys:role:deleteRoleMenu",
              "access": "sys:role:deleteRoleMenu",
              "uri": "/sys/role/deleteRoleMenu",
              "name": "删除角色菜单关系",
              "remark": "删除角色菜单关系",
              "sort": 0
            }
          ]
        },
        {
          "id": "sys:role:index",
          "access": "sys:role:index",
          "uri": "/sys/role/index",
          "name": "角色管理",
          "remark": "角色管理",
          "sort": 0,
          "children": [
            {
              "id": "sys:role:remove",
              "access": "sys:role:remove",
              "uri": "/sys/role/remove",
              "name": "删除角色",
              "remark": "删除角色",
              "sort": 0
            },
            {
              "id": "sys:role:get",
              "access": "sys:role:get",
              "uri": "/sys/role/get",
              "name": "通过id获取角色",
              "remark": "通过id获取角色",
              "sort": 0
            },
            {
              "id": "sys:role:update",
              "access": "sys:role:update",
              "uri": "/sys/role/update",
              "name": "修改角色",
              "remark": "修改角色",
              "sort": 0
            },
            {
              "id": "sys:role:list",
              "access": "sys:role:list",
              "uri": "/sys/role/list",
              "name": "分页查询角色",
              "remark": "分页查询角色",
              "sort": 0
            },
            {
              "id": "sys:role:save",
              "access": "sys:role:save",
              "uri": "/sys/role/save",
              "name": "添加角色",
              "remark": "添加角色",
              "sort": 0
            }
          ]
        },
        {
          "id": "sys:user:index",
          "access": "sys:user:index",
          "uri": "/sys/user/index",
          "name": "用户管理",
          "remark": "用户管理",
          "sort": 0,
          "children": [
            {
              "id": "sys:user:remove",
              "access": "sys:user:remove",
              "uri": "/sys/user/remove",
              "name": "删除用户",
              "remark": "删除用户",
              "sort": 0
            },
            {
              "id": "sys:user:get",
              "access": "sys:user:get",
              "uri": "/sys/user/get",
              "name": "通过id获取用户",
              "remark": "通过id获取用户",
              "sort": 0
            },
            {
              "id": "sys:user:update",
              "access": "sys:user:update",
              "uri": "/sys/user/update",
              "name": "更新用户",
              "remark": "更新用户",
              "sort": 0
            },
            {
              "id": "sys:user:list",
              "access": "sys:user:list",
              "uri": "/sys/user/list",
              "name": "分页查询用户列表",
              "remark": "分页查询用户列表",
              "sort": 0
            },
            {
              "id": "sys:user:save",
              "access": "sys:user:save",
              "uri": "/sys/user/save",
              "name": "添加用户",
              "remark": "添加用户",
              "sort": 0
            }
          ]
        }
      ]
    }
  ]
}
```



## 小结

本文主要是做了基于swaggerui的权限注解扫描收集，并完成了权限管理的相关接口，目前为止，还没有把登录和拦截器的内容加上，下一篇会将这个补上。

## 项目源码地址

- 后端

 https://gitee.com/mldong/mldong

- 前端

 https://gitee.com/mldong/mldong-vue

## 相关文章


 [打造一款适合自己的快速开发框架-先导篇](https://juejin.im/post/5eca0304518825432978055c) 

[打造一款适合自己的快速开发框架-后端脚手架搭建](https://juejin.im/post/5eca05206fb9a047e16c7b3c) 

 [打造一款适合自己的快速开发框架-集成mapper](https://juejin.im/post/5eca484551882543345e81f4) 

 [打造一款适合自己的快速开发框架-集成swaggerui和knife4j](https://juejin.im/post/5eca68d56fb9a04802146091) 

 [打造一款适合自己的快速开发框架-通用类封装之统一结果返回、统一异常处理](https://juejin.im/post/5ed10fb16fb9a047aa65f33b) 

 [打造一款适合自己的快速开发框架-业务错误码规范及实践](https://juejin.im/post/5ed1f623e51d457890602b62) 

 [打造一款适合自己的快速开发框架-框架分层及CURD样例](https://juejin.im/post/5ed30ae0e51d45788c739711) 

 [打造一款适合自己的快速开发框架-mapper逻辑删除及枚举类型规范](https://juejin.im/post/5ed363dc6fb9a047d112719c) 

 [打造一款适合自己的快速开发框架-数据校验之Hibernate Validator](https://juejin.im/post/5ed3a24c6fb9a047ba31fce7) 

 [打造一款适合自己的快速开发框架-代码生成器原理及实现](https://juejin.im/post/5eda67c651882543306822df) 

 [打造一款适合自己的快速开发框架-通用查询设计与实现](https://juejin.im/post/5edb82736fb9a047fe5c0aad) 