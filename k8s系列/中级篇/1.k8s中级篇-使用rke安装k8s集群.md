## 前言

之前为了快速入门学习k8s的编排，直接使用了阿里云上的k8s集群，本中级篇这里尝试一下从0-1搭建k8s集群，并演示一些更为经典的案例：如自建nfs服务器存储卷的使用、自动证书签发cert-manager let's encrypt、rancher2.x、helm包管理工具等。

## 环境

| 主机名   | ip            | 角色   |
| -------- | ------------- | ------ |
| mldong01 | 192.168.0.245 | master |
| mldong02 | 192.168.0.54  | node01 |
| mldong03 | 192.168.0.22  | node02 |

三台主机为华为软开云的ECS，CentOS Linux release 7.6.1810 (Core) 

## 安装docker

版本 `19.03.9`

1. 卸载当前docker版本（按需）

``` shell
yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

2. 安装包管理工具

   ``` shell
   yum install -y yum-utils
   yum-config-manager \
       --add-repo \
       https://download.docker.com/linux/centos/docker-ce.repo
   ```

3. 查看版本

   ``` shell
   yum list docker-ce --showduplicates | sort -r
   ```

4. 安装docker-指定版本

   ``` shell
   yum install docker-ce-19.03.9-3.el7 docker-ce-cli-19.03.9-3.el7 containerd.io
   ```

5. 启动docker

   ``` shell
   systemctl start docker
   ```

6. 修改配置

   ``` lua
   {
     "registry-mirrors": [
       "https://3p42xjxk.mirror.aliyuncs.com",
       "https://registry.docker-cn.com",
       "http://hub-mirror.c.163.com",
       "https://docker.mirrors.ustc.edu.cn"
     ],
     "log-driver": "json-file",
     "log-opts": {
       "max-size": "100m"
     }
   }
   
   ```
   
* registry-mirrors：加速镜像
   * log-driver：日志引擎
   * log-opts：日志配置
   
7. 重启docker

   ``` shell
   systemctl restart docker
   ```

## 安装Kubectl

* 源配置

``` shell
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```

* 安装

``` shell
[root@mldong ~]# yum install -y kubectl
```

## 安装rke前准备

1. 禁用所有 woker 节点上的交换功能（Swap）

   ``` shell
   swapoff -a
   ```

2. 检查下列模组是否存在-所有节点

   ``` shell
   for module in br_netfilter ip6_udp_tunnel ip_set ip_set_hash_ip ip_set_hash_net iptable_filter iptable_nat iptable_mangle iptable_raw nf_conntrack_netlink nf_conntrack nf_conntrack_ipv4   nf_defrag_ipv4 nf_nat nf_nat_ipv4 nf_nat_masquerade_ipv4 nfnetlink udp_tunnel veth vxlan x_tables xt_addrtype xt_conntrack xt_comment xt_mark xt_multiport xt_nat xt_recent xt_set  xt_statistic xt_tcpudp;
       do
         if ! lsmod | grep -q $module; then
           echo "module $module is not present";
         fi;
       done
   ```

3. 修改sysctl配置-所有节点

   ``` shell
   vi /etc/sysctl.conf
   ## 加入如下
   net.bridge.bridge-nf-call-iptables=1
   ## 重新加载配置
   sysctl -p /etc/sysctl.conf
   ```

4. 创建用户-所有节点

   ``` shell
   # 创建用户
   useradd rkeuser
   # 给用户设置密码
   passwd rkeuser
   ```

5. 将用户添加到docker组

   ``` shell
   usermod -aG docker rkeuser
   ```

6. 配置免密登录-master节点

   ``` shell
   # 生成公钥-私钥对
   ssh-keygen  -t rsa -C 'dev@mldong.com'
   # 将master节点的公钥复制到所有节点（包括自身）-使得新建的rkeuser用户
   ssh-copy-id rkeuser@192.168.0.245
   ssh-copy-id rkeuser@192.168.0.54
   ssh-copy-id rkeuser@192.168.0.22
   ```

7. 验证rkeuser是否有docker命令权限

   ``` shell
   # 登录
   ssh rkeuser@192.168.0.245
   # 执行docker命令
   docker ps
   ```

8. 端口要求

   开放`6443`-KubeAPI和`2379`-etcd

9. SSH server配置

   ``` shell
   vi /etc/ssh/sshd_config
   
   ## 允许TCP转发
   AllowTcpForwarding yes
   ```

## 安装介绍

1. 下载rke二进制包-master

   `https://github.com/rancher/rke/releases`

   ``` shell
   wget https://github.com/rancher/rke/releases/download/v1.2.4-rc9/rke_linux-amd64
   ```

2. 修改文件名并执行运行权限

   ``` shell
   mv rke_linux-amd64 /usr/local/bin/rke
   chmod +x /usr/local/bin/rke
   ```

3. 查看版本号

   ``` shell
   [root@mldong01 download]# rke --version
   rke version v1.2.4-rc9
   ```

4. 使用rke生成配置文件

   ``` shell
   rke config --name cluster.yml
   ```

   也可以使用下面的样例文件

   ``` yaml
   nodes:
   - address: 192.168.0.245
     port: "22"
     internal_address: 192.168.0.245
     role:
     - controlplane
     - worker
     - etcd
     hostname_override: "mldong01"
     user: rkeuser
     ssh_key_path: ~/.ssh/id_rsa
     ssh_agent_auth: true
     labels: {}
     taints: []
   - address: 192.168.0.54
     port: "22"
     internal_address: 192.168.0.54
     role:
     - worker
     hostname_override: "mldong02"
     user: rkeuser
     ssh_key_path: ~/.ssh/id_rsa
     ssh_agent_auth: true
     labels: {}
     taints: []
   - address: 192.168.0.22
     port: "22"
     internal_address: 192.168.0.22
     role:
     - worker
     hostname_override: "mldong03"
     user: rkeuser
     ssh_key_path: ~/.ssh/id_rsa
     ssh_agent_auth: true
     labels: {}
     taints: []
   kubernetes_version: "v1.19.6-rancher1-1"
   cluster_name: "mldong-k8s"
   ```

   主要参数说明：

   nodes[].address：对外ip

   nodes[].port：ssh端口号

   nodes[].internal_address：内网ip

   nodes[].role：节点角色，数组，三个选项[controlplane,worker,etcd]

   nodes[].hostname_override：虚拟域名

   nodes[].user：ssh用户名

   nodes[].ssh_key_path：ssh私钥

   nodes[].ssh_agent_auth：启用ssh认证

   kubernetes_version：k8s版本，可以使用命令查看支持版本`rke config --list-version --all`

   cluster_name：集群名称

5. 开始执行安装

   ``` shell
   rke up --config cluster.yml
   ```

   ![image-20201229162725543](http://qiniu.mldong.com/mldong-article/images/image-20201229162725543.png)

   安装过程需要下载镜像，时间比较久，请耐心等待

   ![image-20201229175001440](http://qiniu.mldong.com/mldong-article/images/image-20201229175001440.png)

6.  安装成功后会生成相关文件

   - `cluster.yml`：RKE 集群的配置文件。
   - `kube_config_cluster.yml`：该集群的[](https://docs.rancher.cn/docs/rke/kubeconfig/_index)包含了获取该集群所有权限的认证凭据。
   - `cluster.rkestate`：[Kubernetes 集群状态文件](https://docs.rancher.cn/docs/rke/installation/_index#Kubernetes-集群状态文件)，包含了获取该集群所有权限的认证凭据，使用 RKE v0.2.0 时才会创建这个文件。

7. 验证安装

   * 复制kubeconfig文件

     ``` shell
     scp kube_config_cluster.yml ~/.kube/config
     ```

   * 获取集群节点信息

     ``` shell
     [root@mldong01 download]# kubectl get nodes
     NAME       STATUS   ROLES                      AGE     VERSION
     mldong01   Ready    controlplane,etcd,worker   3d16h   v1.19.6
     mldong02   Ready    worker                     3d16h   v1.19.6
     mldong03   Ready    worker                     3d16h   v1.19.6
     ```


## 完全卸载rke

``` shell
#!/bin/bash
#杀死所有正在运行的容器
docker stop $(docker ps -a -q)

#删除所有容器
docker rm -f $(docker ps -qa)

#删除所有容器卷
  docker volume rm $(docker volume ls -q)
  
#卸载mount目录
  for mount in $(mount | grep tmpfs | grep '/var/lib/kubelet' | awk '{ print $3 }') /var/lib/kubelet /var/lib/rancher; do umount $mount; done
  
#删除残留路径
rm -rf /etc/ceph \
       /etc/cni \
       /etc/kubernetes \
       /opt/cni \
       /opt/rke \
       /run/secrets/kubernetes.io \
       /run/calico \
       /run/flannel \
       /var/lib/calico \
       /var/lib/etcd \
       /var/lib/cni \
       /var/lib/kubelet \
       /var/lib/rancher/rke/log \
       /var/log/containers \
       /var/log/pods \

#清理网络接口
  network_interface=`ls /sys/class/net`
  for net_inter in $network_interface;
  do
    if ! echo $net_inter | grep -qiE 'lo|docker0|eth*|ens*';then
      ip link delete $net_inter
    fi
  done
  
#清理残留进程
  port_list='80 443 6443 2376 2379 2380 8472 9099 10250 10254'

  for port in $port_list
  do
    pid=`netstat -atlnup|grep $port |awk '{print $7}'|awk -F '/' '{print $1}'|grep -v -|sort -rnk2|uniq`
    if [[ -n $pid ]];then
      kill -9 $pid
    fi
  done
  
  pro_pid=`ps -ef |grep -v grep |grep kube|awk '{print $2}'`

  if [[ -n $pro_pid ]];then
    kill -9 $pro_pid
  fi
#清理路由规则 
sudo iptables --flush
sudo iptables --flush --table nat
sudo iptables --flush --table filter
sudo iptables --table nat --delete-chain
sudo iptables --table filter --delete-chain
# 重启docker
sudo systemctl restart docker
```

## 小结

使用rke安装隐藏了一些k8s的安装细节，其他更详细的说明建议去看rancher2的官方文档，这里就不展开。

[rancher官方文档]: http://docs.rancher.cn/	"rancher官方文档"

## 相关文章

[ k8s中级篇-Helm安装与入门](https://juejin.cn/post/6912070528534577159)

[ k8s中级篇-cert-manager+Let's Encrypt自动证书签发](https://juejin.cn/post/6912071027954565133)

[k8s中级篇-Helm安装nfs-client-provisioner](https://juejin.cn/post/6912071173413011470)

