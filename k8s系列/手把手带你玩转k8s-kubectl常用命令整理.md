## 前言

当我们使用云平台的容器服务后，按理来说是可以直接使用其页面进行容器管理的，但是为了学习，我们并不能这样做，所以还是得一步一步来。这里先从kubectl的常用命令开始，这也是后续做持续集成的基础。

## Kubectl配置

### 多集群环境配置

上一篇文中，我们将KubeConfig的内容直接复制到/root/.kube/config文件下，这是仅有一个集群时的做法，如果多个集群又如何配置呢？下面简单讲述一下。假设有三个集群a、b、c。配置文件如下：

``` 
├── /root/.kube
	├── config
	├── a-config
	├── b-config
	└── c-config
```

新增环境变量KUBECONFIG

``` shell
[root@mldong ~]# vi /root/.bashrc
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/a-config:$HOME/.kube/b-config:$HOME/.kube/c-config:$HOME/.kube/config
```

使用环境变量生效

``` shell
[root@mldong ~]# source /root/.bashrc
```

查看环境变量

``` shell
[root@mldong ~]# echo $KUBECONFIG
:/root/.kube/a-config:/root/.kube/b-config:/root/.kube/c-config:/root/.kube/config
```

### 配置相关命令

#### 查看帮助

``` shell
[root@mldong ~]# kubectl config --help
Available Commands:
  current-context Displays the current-context  显示当前环境
  delete-cluster  Delete the specified cluster from the kubeconfig
  delete-context  Delete the specified context from the kubeconfig
  get-clusters    Display clusters defined in the kubeconfig
  get-contexts    Describe one or many contexts
  rename-context  Renames a context from the kubeconfig file.
  set             Sets an individual value in a kubeconfig file
  set-cluster     Sets a cluster entry in kubeconfig
  set-context     Sets a context entry in kubeconfig
  set-credentials Sets a user entry in kubeconfig
  unset           Unsets an individual value in a kubeconfig file
  use-context     Sets the current-context in a kubeconfig file
  view            Display merged kubeconfig settings or a specified kubeconfig file

Usage:
  kubectl config SUBCOMMAND [options]

Use "kubectl <command> --help" for more information about a given command.
Use "kubectl options" for a list of global command-line options (applies to all commands).
```

#### 查看集群列表

``` shell
[root@mldong ~]# kubectl config get-contexts
```

带*则为当前管理的集群

#### 显示当前环境

``` shell
[root@mldong ~]# kubectl config current-context
kubernetes-admin-ca6e7a9431a0140d2bbb9c1e1a16bf3fd
```

#### 切换环境

``` shell
[root@mldong ~]# kubectl config use-context k8s-dev
Switched to context "k8s-dev".
```

## Kubectl常用命令

因为命令都有共性，所以这里的常用命令都只是简单罗列。多练多看--help。

### 查询类get

#### 获取所有节点

``` shell
[root@mldong nginx]# kubectl get node
NAME                           STATUS   ROLES    AGE   VERSION
cn-zhangjiakou.172.26.22.118   Ready    <none>   83m   v1.16.9-aliyun.1
cn-zhangjiakou.172.26.22.119   Ready    <none>   83m   v1.16.9-aliyun.1
```

#### 获取所有命名空间

``` shell
[root@mldong ~]# kubectl get namespace
或
[root@mldong ~]# kubectl get ns
NAME              STATUS   AGE
default           Active   36d
kube-node-lease   Active   36d
kube-ops          Active   6d11h
kube-public       Active   36d
kube-system       Active   36d
mldong-test      Active   1h
```

#### 获取默认命名空间下的所有pod

``` shell
[root@mldong ~]# kubectl get pods
NAME                                      READY   STATUS    RESTARTS   AGE
myapp-backend-pod-798dc9b584-9f9qc        1/1     Running   0          5h35m
myapp-backend-pod-798dc9b584-q4bll        1/1     Running   0          5h35m
nfs-client-provisioner-698b6b7c6c-hf4pc   1/1     Running   0          2d2h
```

#### 获取指定命名空间下的所有pod

``` shell
[root@mldong ~]# kubectl get pods -n kube-system
```

#### 获取所有命名空间下的所有pod

``` shell
[root@mldong ~]# kubectl get pods -A
```

#### 获取默认命名空间下的所有Service

``` shell
kubectl get pods
```

#### 获取指定命名空间下的所有Service

``` shell
[root@mldong ~]# kubectl get Service -n kube-system
```

#### 获取所有Service

``` shell
[root@mldong ~]# kubectl get Service -A
```

#### 输出指定命名空间下的所有Service的yaml

``` shell
kubectl get Service -n kube-system -o yaml
```

#### 小结

``` shell
kubectl get [资源名] -n [命名空间]
```

### 详情类describe

和上面的用法一致

#### 获取指定命名空间下的所有Service

``` shell
kubectl describe Service -n kube-system
```

#### 获取指定命名空间下名称为kube-dns的Service

``` shell
kubectl describe Service kube-dns -n kube-system
```

#### 小结

``` shell
kubectl describe [资源名] -n [命名空间]
```

