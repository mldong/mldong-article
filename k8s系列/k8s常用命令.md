### 证书相关

#### 创建证书

``` shell
kubectl create secret tls dypt-ingress-secret --cert=mldong.crt --key=mldong.key -n mldong
```

#### 获取证书信息

```shell
kubectl get secret -n mldong-test
```

#### 查看证书详情

``` shell
kubectl describe secret dypt-ingress-secret -n mldong-test
```

### ConfigMap相关

#### 创建ConfigMap

``` shell
kubectl create configmap mldong-test-cm --from-file=application-test.yml=./admin-config.yml -n mldong-test
```

#### 查看ConfigMap信息

``` shell
kubectl get configmap -n dypt
```

#### 看查ConfigMap详情

``` shell
kubectl describe configmap mldong-test-cm -n dypt
```

#### 删除ConfigMap

``` shell
kubectl delete configmap mldong-test-cm -n mldong-test
```

nginx.ingress.kubernetes.io/rewrite-target

### 进入容器

``` shell
kubectl exec -it pod-name -n mldong-test --  bash
```

#### 查看配置文件是否存在

``` shell
ls /etc/nginx/conf.d/
```





