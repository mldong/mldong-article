## 前言

上一篇使用k8s安装了本地开发服务，但是可能大多数人电脑配置还是不足以支持开启k8s集群的，这里为了方便，也出了一个docker-compose版的yaml。

## 开始操作

### 初始化目录

1. 初始化配置文件

   ``` shell
   mkdir -p /d/mldong/docker-data/mysql57/config
   ```

2. 初始化数据目录

   ``` shell
   mkdir -p /d/mldong/docker-data/mysql57/data
   ```

3. 初始化脚本

   初始化容器后，会执行该目录下的sql脚本，一般用于初始化数据的。该初始化脚本会在如下MYSQL_DATABASE定义的数据库下执行。
   
   ``` shell
   mkdir -p /d/mldong/docker-data/mysql57/init
   ```

### 创建配置文件

``` shell
cat << EOF > /d/mldong/docker-data/mysql57/config/my.cnf
[mysqld]
user=mysql
wait_timeout=2880000
interactive_timeout = 2880000
max_allowed_packet = 256M
default-storage-engine=INNODB
character-set-server=utf8mb4
default-time_zone = '+8:00'
[client]
default-character-set=utf8mb4
[mysql]
default-character-set=utf8mb4
EOF

```

### 创建定义文件

``` shell
cat << EOF > /d/mldong/docker-data/mysql57/docker-compose.yaml
version: '2'
services:
  mysql:
    environment:
      MYSQL_ROOT_PASSWORD: "123456"
      MYSQL_DATABASE: "app"
      MYSQL_USER: "u_app"
      MYSQL_PASSWORD: "app@321"
    image: "mysql:5.7.28"
    restart: always
    volumes:
      - "/d/mldong/docker-data/mysql57/data:/var/lib/mysql"
      - "/d/mldong/docker-data/mysql57/config/my.cnf:/etc/my.cnf"
      - "/d/mldong/docker-data/mysql57/init:/docker-entrypoint-initdb.d/"
    ports:
      - "13306:3306"
EOF

```

### 创建初始化sql脚本

``` shell
cat << EOF > /d/mldong/docker-data/mysql57/init/init.sql
/*!40101 SET NAMES utf8mb4 */;
DROP TABLE IF EXISTS sys_role;
CREATE TABLE sys_role (
  id char(36) CHARACTER SET utf8mb4 NOT NULL COMMENT '主键',
  name varchar(64) CHARACTER SET utf8mb4 NOT NULL COMMENT '角色名称',
  role_type int(6) DEFAULT '10' COMMENT '角色类型(10->管理员|ADMIN,20->工作流角色|WORKFLOW)',
  create_time datetime DEFAULT NULL COMMENT '创建时间',
  update_time datetime DEFAULT NULL COMMENT '更新时间',
  is_deleted tinyint(1) DEFAULT '0' COMMENT '是否删除(1->删除|YES,0->未删除|NO)',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

INSERT INTO sys_role VALUES ('fe287c64b03d4df69c6661224f9f1c01','演示角色',10,'2020-03-11 20:31:50','2020-03-11 20:31:50',0);
EOF

```

### 开始创建实例

``` shell
# 先下载镜像
docker pull mysql:5.7.28
```

``` shell
docker-compose -f /d/mldong/docker-data/mysql57/docker-compose.yaml up -d
```

![image-20200814202948069](http://qiniu.mldong.com/mldong-article/images/image-20200814202948069.png)

![image-20200814202959790](http://qiniu.mldong.com/mldong-article/images/image-20200814202959790.png)

## 验证

![image-20200814203224404](http://qiniu.mldong.com/mldong-article/images/image-20200814203224404.png)

![image-20200814203606869](http://qiniu.mldong.com/mldong-article/images/image-20200814203606869.png)

## docker-compose常用命令说明

1. 运行服务-非后台运行

   ``` shell
   docker-compose -f docker-compose.yaml up
   ```

2. 运行服务-后台运行

   ``` shell
   docker-compose -f docker-compose.yaml up -d
   ```

3. 停止并移除定义的服务

   ``` shell
   docker-compose -f docker-compose.yaml down
   ```

4. 停止正在运行的服务

   ``` shell
   docker-compose -f docker-compose.yaml stop
   ```

5. 启动停止的服务

   ``` shell
   docker-compose -f docker-compose.yaml start
   ```