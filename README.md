## Docker入门篇

[Hyper-V上安装Centos7](https://juejin.im/post/6873810910851547144)

[Centos7上安装Docker](https://juejin.im/post/6873811094965026830)

[Docker安装Mysql](https://juejin.im/post/6873811180184567816)

[Docker安装Redis](https://juejin.im/post/6873811528798339086)

[Docker安装Zookeeper](https://juejin.im/post/6873811710042439687)

## k8s篇

[手把手带你玩转k8s-集群创建和Hello World](https://juejin.im/post/5efde9066fb9a07ead0f3d8d) 

[手把手带你玩转k8s-ConfigMap与持久化存储](https://juejin.im/post/5f0001266fb9a07e8b213873) 

[手把手带你玩转k8s-完整的发布一个可对外访问的服务](https://juejin.im/post/5f01ef5c6fb9a07e7e043114) 

[手把手带你玩转k8s-docker进阶Dockerfile与docker-compose](https://juejin.im/post/5f034522e51d45348e279cf9) 

[手把手带你玩转k8s-一键部署springboot项目](https://juejin.im/post/5f05da9bf265da22dd7dcce4) 

[手把手带你玩转k8s-一键部署vue项目](https://juejin.im/post/5f0890d9f265da22e27a82d8)

[手把手带你玩转k8s-常用对象详解](https://juejin.im/post/5f09e347f265da230f284468) 

[手把手带你玩转k8s-jenkins安装与流水线](https://juejin.im/post/5f0f30806fb9a07e8b214f83) 

[手把手带你玩转k8s-jenkins流水线语法](https://juejin.im/post/5f12fd205188252e734b6d58) 

[手把手带你玩转k8s-jenkins流水线发布springboot项目](https://juejin.im/post/5f158e2a6fb9a07ebb226218) 

[手把手带你玩转k8s-jenkins流水线发布vue项目](https://juejin.im/post/6854573215592349709) 

[手把手带你玩转k8s-健康检查之存活探针与就绪探针](https://juejin.im/post/6856015421155082247)

[手把手带你玩转k8s-win10上搭建k8s集群](https://juejin.im/post/6856407118669742094)

[手把手带你玩转k8s-win10的k8s上搭建开发环境服务](https://juejin.im/post/6858830827757928462/)

## 压测篇

[测试篇-压力测试工具tsung安装与简单使用](https://juejin.im/post/6862660365646954504)

[测试篇-压测工具tsung详解](https://juejin.im/post/6862985848691490824)

## 开源框架-前端篇

 [打造一款适合自己的快速开发框架-先导篇](https://juejin.im/post/5eca0304518825432978055c) 

 [打造一款适合自己的快速开发框架-前端脚手架搭建](https://juejin.im/post/5eca049be51d457848684e16) 

[打造一款适合自己的快速开发框架-前端篇之登录与路由模块化](https://juejin.im/post/5eeb877bf265da02be0ce628)

[打造一款适合自己的快速开发框架-前端篇之框架分层及CURD样例](https://juejin.im/post/5eed4f81f265da02c94e1346) 

[打造一款适合自己的快速开发框架-前端篇之字典组件设计与实现](https://juejin.im/post/5ef2233ce51d45741e436c11) 

[打造一款适合自己的快速开发框架-前端篇之下拉组件设计与实现](https://juejin.im/post/5ef43301f265da22cb481369) 

[打造一款适合自己的快速开发框架-前端篇之选择树组件设计与实现](https://juejin.im/post/5ef563185188252e9a1fe26a) 

 [打造一款适合自己的快速开发框架-前端篇之代码生成器](https://juejin.im/post/5ef6a6b16fb9a07e693a5eae) 

 [打造一款适合自己的快速开发框架-前端篇之权限管理](https://juejin.im/post/5efcb274f265da22df3cc314) 

## 开源框架-后端篇

[打造一款适合自己的快速开发框架-后端脚手架搭建](https://juejin.im/post/5eca05206fb9a047e16c7b3c) 

 [打造一款适合自己的快速开发框架-集成mapper](https://juejin.im/post/5eca484551882543345e81f4) 

 [打造一款适合自己的快速开发框架-集成swaggerui和knife4j](https://juejin.im/post/5eca68d56fb9a04802146091) 

 [打造一款适合自己的快速开发框架-通用类封装之统一结果返回、统一异常处理](https://juejin.im/post/5ed10fb16fb9a047aa65f33b) 

 [打造一款适合自己的快速开发框架-业务错误码规范及实践](https://juejin.im/post/5ed1f623e51d457890602b62) 

 [打造一款适合自己的快速开发框架-框架分层及CURD样例](https://juejin.im/post/5ed30ae0e51d45788c739711) 

 [打造一款适合自己的快速开发框架-mapper逻辑删除及枚举类型规范](https://juejin.im/post/5ed363dc6fb9a047d112719c) 

 [打造一款适合自己的快速开发框架-数据校验之Hibernate Validator](https://juejin.im/post/5ed3a24c6fb9a047ba31fce7) 

 [打造一款适合自己的快速开发框架-代码生成器原理及实现](https://juejin.im/post/5eda67c651882543306822df) 

 [打造一款适合自己的快速开发框架-通用查询设计与实现](https://juejin.im/post/5edb82736fb9a047fe5c0aad) 

 [打造一款适合自己的快速开发框架-基于rbac的权限管理](https://juejin.im/post/5edcf981518825432a35a066) 

 [打造一款适合自己的快速开发框架-登录与权限拦截](https://juejin.im/post/5edf8d17518825433a57c56d) 

 [打造一款适合自己的快速开发框架-http请求日志全局处理](https://juejin.im/post/5ee0f916e51d457b3f4a1ad3) 

 [打造一款适合自己的敏捷开发框架-上传模块设计与实现](https://juejin.im/post/5ee60041f265da76d85d249b) 

## 其他

[基于springboot+vue的前后端分离后项目部署方案](https://juejin.im/post/6844904146282217480)

[Python3接口自动化测试](https://juejin.im/post/6844904147066552333)

[代码生成器原理及实现-Python3版](https://juejin.im/post/6844904147720863751)

[工作流引擎原理-打造一款适合自己的工作流引擎](https://juejin.im/post/6844904167463485453)