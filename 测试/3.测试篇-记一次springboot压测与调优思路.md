##  前言

当我们系统开发完成，从严谨的角度来说，除了进行必要的功能测试外，一些核心的业务也是需要进行压力测试的。然后通过压测来预估服务器所能支撑的最大并发量。当然，我们也可以通过压测来达到系统调优的目地。本文就使用tsung对springboot进行压测，然后提供一些优化思路。

## 压力来源

正常的生产服务，压力来源肯定是外来用户的访问。而测试环境，或者普通的系统可没有那么多用户访问，这种情况只能使用一些压测工具来制造访问压力，主流的压测工具有jmetter,loadRunner,tsung，ab等。这里不对这几个压测工具做比较，我们这里使用tsung。原因是个人觉得其更方便的制作测试压力，特别是在没有界面的linux系统上。使用起来会更加方便。

### 单机模式

单机模式即只有一个压测服务器。

![image-20200824214208303](http://qiniu.mldong.com/mldong-article/images/image-20200824214208303.png)

### 集群模式

一台机器所能产生的压力是有限的，这时候就需要到分布式-集群模式。当然，因为本文都是在本地的虚拟机上操作的，占用的都是同一台机器的服务，就先不演示集群模式了，下一篇文章再拿来单独写。

![image-20200824221829571](http://qiniu.mldong.com/mldong-article/images/image-20200824221829571.png)

## 准备工作

### 环境安装

1. 安装erlang（每台服务器都要安装 ，安装教程参考文章一）

2. 安装tsung（每台服务器都要安装  ，安装教程参考文章一）

3. 配置公钥，使压测机可以使用ssh免密登录自身和待压测的服务器和数据库（记住，要保证known_hosts存在记录，即必须免密登录过一次）

4. `/etc/hosts`新增记录

5. 数据库服务器安装mysql5.7

6. 待压测的服务器安装jdk

   注：app和db需要安装erlang和tsung的原因是要收集监控数据。

**配置片段：**

1. 客户端-即压测机配置

   ``` xml
    <clients>
       <client host="master" use_controller_vm="false" maxusers="30000" cpu="3"></client>
     </clients>
   ```

   * host即上面测试机的master，经测试，使用localhost不需要配置ssh免密，不过为了统一，建议配置ssh免密，然后使用master。
   * 这里建议把`use_controller_vm`设置为`false`，因为如果设置为`true`，当请求的用户数达到maxuser时，压测程序会报错并自动停止。
   * 在tsung里面，一个cpu即一个erlang虚拟机，或者客户机，用于产生用户的。如果想生产用户的能力强一些，就配置得大一些，不过不能超过当前机器cpu核心数。建议配置为当前机器cpu核心数减1。


2. 负载配置-即要压测的服务

   ``` xml
   <servers>
     <server host="app" port="18080" type="tcp" weight="1"></server>
   </servers>
   ```

3. 监控配置

   ``` xml
    <monitoring>
       <monitor host="app" type="erlang"></monitor>
       <monitor host="db" type="erlang"></monitor>
     </monitoring>
   ```

   * host即为要监控的服务器
   * type,默认为erlang.


4. 压力配置，即配置多少并发

   ``` xml
    <load>
      <arrivalphase phase="1" duration="60" unit="minute">
        <users maxnumber="200000" arrivalrate="1300" unit="second"></users>
      </arrivalphase>
     </load>
   ```

   * 以每秒1300个的速度生产用户去访问服务，当然，能不能生产出那么多，和压测机的配置有些关系，主要受cpu和内存影响。
   * 本次压测一共生成200000个用户，在60分钟内完成。如超过60分钟未完成，测试可提前结束。
   
5. 会话测试，即配置要压测的接口

   ``` xml
    <sessions>
     <session name="http-example" probability="100" type="ts_http">
     <transaction name="login">
        <request subst="true">
           <http url="/sys/login" method="POST" contents="{&quot;password&quot;:&quot;mldong@321&quot;,&quot;userName&quot;:&quot;admin&quot;}">
             <http_header name="Content-Type" value="application/json"/>
           </http>
         </request>
      </transaction>
     </session>
    </sessions>
   
   ```

   * 这里只简单配置了一个登录接口
   * 注意contents要进行json压缩，然后html转义可以去该网站进行转换`https://www.sojson.com/`

### 准备一个springboot服务

这里就偷懒直接使用开源的那个 https://gitee.com/mldong/mldong

## 压测用例

本文这里以登录接口进行压测用例说明。先从上面取如下参数：

* load.users.maxnumber：总请求数
* load.users.arrivalrate： 每秒生成的请求数

当然，除了压测参数，还有springboot内嵌的tomcat的常用配置

* server.tomcat.max-threads：最大工作线程数，默认200
* server.tomcat.max-connections： 最大可被连接数，默认为10000
* server.tomcat.min-spare-threads：最小工作空闲线程数，默认10

还有数据库连接池常用配置：

* spring.datasource.initial-size：初始化时建立物理连接的个数，默认0
* spring.datasource.max-active：最大连接池数量，默认值8
* spring.datasource.max-idle： 据说已被启用，配置已无效，默认值8
* spring.datasource.min-idle：最小连接池数，默认0

当然还有我个的日志配置：

* 只输出ERROR级别日志
* 只输出请求日志-同步
* 只输出请求日志-异步

注：以上只取了主要的配置参数，并不是全部。

为了简单，我们这里只进行并发数测试，所以，其他都取默认值 ，然后arrivalrate，从200开始，以200为一级进行递增。如下表：

| 并发数 | 平均每个请求响应时间 | 平均请求速率(TPS) | 应用CPU使用率最大值 | 应用CPU使用率最小值 |
| ------ | -------------------- | ----------------- | ------------------- | ------------------- |
| 200    |                      |                   |                     |                     |
| 400    |                      |                   |                     |                     |
| 600    |                      |                   |                     |                     |
| 800    |                      |                   |                     |                     |
| 1000   |                      |                   |                     |                     |
| 1200   |                      |                   |                     |                     |
| 1400   |                      |                   |                     |                     |
| 1600   |                      |                   |                     |                     |
| 1800   |                      |                   |                     |                     |

其他配置项：

| 配置项                          | 默认值 | 说明                       |
| ------------------------------- | ------ | -------------------------- |
| load.users.maxnumber            | 200000 | 总请求数                   |
| server.tomcat.max-threads       | 200    | 最大工作线程数             |
| server.tomcat.max-connections   | 10000  | 最大可被连接数             |
| server.tomcat.min-spare-threads | 10     | 最小工作空闲线程数         |
| spring.datasource.initial-size  | 0      | 初始化时建立物理连接的个数 |
| spring.datasource.max-active    | 8      | 最大连接池数量             |
| spring.datasource.min-idle      | 0      | 最小连接池数               |

## 开始压测

